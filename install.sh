##
## FreeIPA Nginx Proxy Walkthrough
##
## install.sh
##

## Variables
ROOT_DOMAIN='ccnyhub.org'
CLIENT_NUM='01'
CLIENT_HOSTNAME="jh-${CLIENT_NUM}"
VM_NAME_IPA_SERVER='freeipa'
VM_NAME_CLIENT="jh${CLIENT_NUM}"

# Step-1 - host
bash -x step-1-host.sh && \

## Step-2 - freeipa-server
vagrant up ${VM_NAME_IPA_SERVER} && \
vagrant ssh ${VM_NAME_IPA_SERVER} -c "bash -x /vagrant/step-2-freeipa_server.sh ${ROOT_DOMAIN}" && \

## Step-3 - certbot on host
bash -x step-3-certbot_on_host.sh ${ROOT_DOMAIN} && \

## Step-4+5 - mokey
## Cannot be fully automated, so run steps by hand
## Add secrets (passwords/keys)
#     $ vim mokey.yaml
## Install FreeIPA client
#     $ vagrant ssh mokey -c "bash -x /vagrant/step-4-freeipa_client.sh centos ${ROOT_DOMAIN} mokey"
## SSH into VM and run commands, one-by-one using vim
#     $ vagrant ssh mokey
#     $ vim /vagrant/step-5-mokey.sh

## Step-4+6 - jupyterhub
vagrant up ${VM_NAME_CLIENT} && \
vagrant ssh ${VM_NAME_CLIENT} -c "bash -x /vagrant/step-4-freeipa_client.sh ubuntu ${ROOT_DOMAIN} ${CLIENT_HOSTNAME} && bash -x /vagrant/step-6-jupyterhub.sh ${ROOT_DOMAIN} ${CLIENT_NUM}"
