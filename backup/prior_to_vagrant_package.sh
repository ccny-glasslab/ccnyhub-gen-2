##
## prior_to_vagrant_package.sh
##

wget https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub
cat vagrant.pub >> ~/.ssh/authorized_keys
rm vagrant.pub
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys
chown -R vagrant:vagrant ~/.ssh
