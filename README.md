# CCNY Hub gen-2

Installs/configures multiple JupyterHub's that use FreeIPA for
authentication (see the ['System components' section][system-components]
for more details about the used technologies).

### Command line steps (run this on target machine)
```bash
## Apply network config
sudo cp /etc/netplan/ ~/netplan_original
sudo rm /etc/netplan/*
sudo cp /mnt/thumb/network_configs/99_config.yaml /etc/netplan
sudo netplan apply

## Install basic apps, clone gist, execute gist install script
sudo apt update
sudo apt install -y git vim tmux
git clone https://gitlab.com/ccny-glasslab/ccnyhub-gen-2 ~/ccnyhub-gen-2
bash -x ~/ccnyhub-gen-2/install.sh
```

### System components

* [Jupyter Hub][tool-01-jh]
* [FreeIPA][tool-02-freeipa]
* [Mokey][tool-03-mokey]
* [Nginx][tool-04-nginx]
* [Let's Encrypt][tool-05-letsencrypt]
* [Ubuntu][tool-06-ubuntu]
* [CentOS][tool-07-centos]
* [VirtualBox][tool-08-virtualbox]
* [Vagrant][tool-09-vagrant]
* [Tmux][tool-10-tmux]


### References
1. [FreeIPA server install guide for CentOS 7 - ComputingforGeeks.com][geeks-freeipa-server-guide]
1. [FreeIPA client install guide for Ubuntu/CentOS - ComputingforGeeks.com][geeks-freeipa-client-guide]
1. [Blog post about HTTPS behind a HTTPS Nginx proxy - reinout.vanrees.org][nginx-https-behind-https-post]
1. [Nginx install guide for Ubuntu - DigitalOcean.com][digitalocean-nginx-ubuntu]
1. [Firewall config guide for Ubuntu - DigitalOcean.com][digitalocean-ubuntu-firewall]
1. [Firewall config guide for CentOS - DigitalOcean.com][digitalocean-centos-firewall]

[system-components]: #system-components
[tool-01-jh]: https://jupyterhub.readthedocs.io/en/stable/
[tool-02-freeipa]: https://www.freeipa.org/
[tool-03-mokey]: https://github.com/ubccr/mokey
[tool-04-nginx]: https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/
[tool-05-letsencrypt]: https://letsencrypt.org/
[tool-06-ubuntu]: https://ubuntu.com/
[tool-07-centos]: https://centos.org/
[tool-08-virtualbox]: https://www.virtualbox.org/
[tool-09-vagrant]: https://www.vagrantup.com
[tool-10-tmux]: https://github.com/tmux/tmux
[geeks-freeipa-server-guide]: https://computingforgeeks.com/install-freeipa-server-centos-7/
[geeks-freeipa-client-guide]: https://computingforgeeks.com/how-to-configure-freeipa-client-on-ubuntu-18-04-ubuntu-16-04-centos-7/
[nginx-https-behind-https-post]: https://reinout.vanrees.org/weblog/2017/05/02/https-behind-proxy.html
[digitalocean-nginx-ubuntu]: https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04
[digitalocean-ubuntu-firewall]: https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-ubuntu-18-04
[digitalocean-centos-firewall]: https://www.digitalocean.com/community/tutorials/additional-recommended-steps-for-new-centos-7-servers#configuring-a-basic-firewall
