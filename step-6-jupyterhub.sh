##
## FreeIPA Nginx Proxy Walkthrough
##
## step-6-jupyterhub.sh
##

## Handle arguments
if [ $# -ln 1 ]; then
  echo 'Missing argument for JH Number, eg. 01, 02'
fi

## Variables
ROOT_DOMAIN="$1"
JH_NUM="$2"
JH_CONFIG_DIR='/etc/jupyterhub'
JH_CONFIG_FILE="${JH_CONFIG_DIR}/jupyterhub_config.py"
BASE_URL="/jh-${JH_NUM}"
HOSTNAME="jh-${JH_NUM}.${ROOT_DOMAIN}"

echo "127.0.0.1 localhost ${HOSTNAME}" | sudo tee -a /etc/hosts
sudo apt update
sudo apt install -y python3-venv
sudo python3 -m venv /opt/jupyterhub/
sudo /opt/jupyterhub/bin/python3 -m pip install wheel
sudo /opt/jupyterhub/bin/python3 -m pip install jupyterhub jupyterlab
sudo /opt/jupyterhub/bin/python3 -m pip install ipywidgets

sudo apt install -y nodejs npm
sudo npm install -g configurable-http-proxy

sudo mkdir -p /opt/jupyterhub/etc/jupyterhub/
cd /opt/jupyterhub/etc/jupyterhub/
sudo /opt/jupyterhub/bin/jupyterhub --generate-config
echo "c.Spawner.default_url = '/lab'" | sudo tee -a /opt/jupyterhub/etc/jupyterhub/jupyterhub_config.py
echo "c.JupyterHub.bind_url = 'http://:8000/jupyter'" | sudo tee -a /opt/jupyterhub/etc/jupyterhub/jupyterhub_config.py

sudo mkdir -p /opt/jupyterhub/etc/systemd
sudo bash -c "cat > /opt/jupyterhub/etc/systemd/jupyterhub.service" <<EOF
[Unit]
Description=JupyterHub
After=syslog.target network.target

[Service]
User=root
Environment="PATH=/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/opt/jupyterhub/bin"
ExecStart=/opt/jupyterhub/bin/jupyterhub -f /opt/jupyterhub/etc/jupyterhub/jupyterhub_config.py

[Install]
WantedBy=multi-user.target
EOF
sudo ln -s /opt/jupyterhub/etc/systemd/jupyterhub.service /etc/systemd/system/jupyterhub.service
sudo systemctl daemon-reload
sudo systemctl enable jupyterhub.service
sudo systemctl start jupyterhub.service
