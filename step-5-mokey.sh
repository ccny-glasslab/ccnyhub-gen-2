##
## FreeIPA Nginx Proxy Walkthrough
##
## mokey.sh
##

## Handle arguments
if [ $# -eq 0 ]; then
  echo "Missing argument for root domain"
  exit 1
fi

ROOT_DOMAIN="$1"
IPA_SUBDOMAIN="ipa.${ROOT_DOMAIN}"
MOKEY_INSTALLER='mokey-0.5.3-1.el7.x86_64.rpm'
MOKEY_DOWNLOAD_URL="https://github.com/ubccr/mokey/releases/download/v0.5.3/${MOKEY_INSTALLER}"
MOKEY_CONFIG="mokey.yaml"

## Start script in a directory with write access
cd /vagrant

## Download, install mokey
sudo yum install -y wget
wget "${MOKEY_DOWNLOAD_URL}" -O ${MOKEY_INSTALLER}
sudo yum install -y mariadb-server
sudo systemctl restart mariadb
sudo systemctl enable mariadb
sudo rpm -Uvh "${MOKEY_INSTALLER}"

## Run these commands by hand, not by script 
## NOTE: substitute [mypass] with a real password
# sudo mysqladmin -u root password
# mysql -u root -p
# mysql> create database mokey;
# mysql> grant all on mokey.* to root@localhost identified by '[mypass]';
# mysql> exit
# mysql -u root -p mokey < /usr/share/mokey/ddl/schema.sql

## Connect mokey to FreeIPA
sudo mkdir /etc/mokey/keytab
sudo kinit admin
sudo ipa role-add 'Mokey User Manager' --desc='Mokey User management'
sudo ipa role-add-privilege 'Mokey User Manager' --privilege='User Administrators'
sudo ipa user-add mokeyapp --first Mokey --last App
sudo ipa role-add-member 'Mokey User Manager' --users=mokeyapp
sudo ipa-getkeytab -s "${IPA_SUBDOMAIN}" -p mokeyapp -k /etc/mokey/keytab/mokeyapp.keytab

## Do something about mokey internals
sudo chmod 640 /etc/mokey/keytab/mokeyapp.keytab
sudo chgrp mokey /etc/mokey/keytab/mokeyapp.keytab

## Copy mokey config file into place
if [ ! -f "${MOKEY_CONFIG}" ]; then
  echo "Error: script asset not found"
  exit 1
fi
sudo cp "${MOKEY_CONFIG}" /etc/mokey/mokey.yaml

## Run mokey
sudo systemctl restart mokey
sudo systemctl enable mokey
