##
## FreeIPA Nginx Proxy Walkthrough
##
## freeipa_client.sh
##

## Handle arguments
if [ $# -lt 3 ]; then
  echo "Error: three arguments required: 1) distro (centos or ubuntu), 2) root domain, 3) client hostname"
  exit 1
fi

## Variables
DISTRO="$1"
ROOT_DOMAIN="$2"
CLIENT_FULL_HOSTNAME="$3.${ROOT_DOMAIN}"
SERVER_DOMAIN="ipa.${ROOT_DOMAIN}"
SERVER_DOMAIN_ALIAS='ipa'
SERVER_IP_ADDRESS='192.168.2.2'
# Uppercase the root domain
# Reference:
# https://bash.cyberciti.biz/guide/Howto:_convert_string_to_all_uppercase
SERVER_REALM=`echo "${ROOT_DOMAIN}" | tr [a-z] [A-Z]`

# TODO make this command idempotent
echo "${SERVER_IP_ADDRESS} ${SERVER_DOMAIN} ${SERVER_DOMAIN_ALIAS}" | sudo tee -a /etc/hosts
sudo hostnamectl set-hostname "${CLIENT_FULL_HOSTNAME}"

## Install installer
if [ "$DISTRO" == 'centos' ]; then
  sudo yum install -y ipa-client tmux vim
elif [ "$DISTRO" == 'ubuntu' ]; then
  sudo apt-get update
  sudo apt-get install -y freeipa-client tmux vim
  ## Apply home directory creation fix needed for Ubuntu clients
  sudo bash -c "cat > /usr/share/pam-configs/mkhomedir" <<EOF
Name: activate mkhomedir
Default: yes
Priority: 900
Session-Type: Additional
Session:
required pam_mkhomedir.so umask=0022 skel=/etc/skel
EOF
  sudo pam-auth-update
else
  echo "Error: unrecognized disto specified. Valid distro: centos, ubuntu."
  exit 1
fi

## Run installer
sudo ipa-client-install \
--hostname="${CLIENT_FULL_HOSTNAME}" \
--mkhomedir \
--force-join \
--server="${SERVER_DOMAIN}" \
--domain="${ROOT_DOMAIN}" \
--realm="${SERVER_REALM}"
