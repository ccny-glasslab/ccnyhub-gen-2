##
## FreeIPA Nginx Proxy Walkthrough
##
## step-1-host.sh
##
## [*] means a substep that is specific to CCNY-JH-K8S project, so if
## you're not part of that project you can skip the substep
##

VAGRANT_PATH=/opt/vagrant
VAGRANT_BIN_PATH=${VAGRANT_PATH}/vagrant

## Start script in a directory with write access
cd ~

## [*] Mount backup of old TLJH profiles
sudo mkdir /mnt/backup
echo "UUID=1334c7af-4189-40a5-a56f-f920a80edd2d /mnt/backup ext4 defaults 0 0" | sudo tee -a /etc/fstab
sudo mount -a

## Tmuxp
sudo apt install -y python3-pip
pip3 install -U tmuxp

## Install Docker
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update
apt-cache policy docker-ce
sudo apt -y install docker-ce
sudo usermod -aG docker ${USER}
# TODO: before docker can be used without sudo, you need to logout and
# log back in

## Install VirtualBox
echo 'deb https://download.virtualbox.org/virtualbox/debian bionic contrib' | sudo tee -a /etc/apt/sources.list
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
sudo apt update
sudo apt install -y virtualbox-6.0

## Install Vagrant
sudo mkdir ${VAGRANT_PATH}
sudo apt install -y zip unzip
wget https://releases.hashicorp.com/vagrant/2.2.5/vagrant_2.2.5_linux_amd64.zip
unzip vagrant_2.2.5_linux_amd64.zip
rm vagrant_2.2.5_linux_amd64.zip
sudo mv vagrant ${VAGRANT_BIN_PATH}
echo -e '# Vagrant\n# Added by bla on '`date`'\nPATH=$PATH:/opt/vagrant' >> ~/.bashrc
source ~/.bashrc
${VAGRANT_BIN_PATH} plugin install vagrant-vbguest vagrant-disksize

## Install, configure Nginx
sudo apt update
sudo apt install -y nginx
cp -r /etc/nginx nginx.orig
if [ ! -d nginx_config/nginx-pre_certbot ]; then
  echo 'Error: script asset not found.'
  exit 1
fi
sudo cp -r nginx_config/nginx-pre_certbot/* /etc/nginx
sudo ln -s -t /etc/nginx/sites-enabled /etc/nginx/sites-available/ccnyhub
sudo unlink /etc/nginx/sites-enabled/default
sudo systemctl restart nginx

## Setup firewall
sudo ufw app list
sudo ufw allow 'OpenSSH' 'Nginx Full'
sudo ufw enable
sudo ufw status
