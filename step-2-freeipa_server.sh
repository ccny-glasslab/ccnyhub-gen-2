##
## FreeIPA Nginx Proxy Walkthrough
##
## step-2-freeipa_server.sh
##
## [*] means a step that is specific to CCNY-JH-K8S project, so if you're
## not part of that project you can skip the step
##

## Handle arguments
if [ $# -eq 0 ]; then
  echo "Missing argument for root domain"
  exit 1
fi

ROOT_DOMAIN="$1"
SERVER_DOMAIN="ipa.${ROOT_DOMAIN}"
SERVER_IP_ADDRESS='192.168.2.2'

## Check for latest OS packages
sudo yum -y install epel-release
sudo yum -y update

## Install basic apps
sudo yum -y install bind-utils vim tmux wget

## Configure hostname / DNS lookups
# TODO Make this command idempotent
echo "${SERVER_IP_ADDRESS} ${SERVER_DOMAIN}" | sudo tee -a /etc/hosts
sudo hostnamectl set-hostname ${SERVER_DOMAIN}

## Install FreeIPA server (there will be prompts)
sudo yum install -y ipa-server
sudo ipa-server-install
